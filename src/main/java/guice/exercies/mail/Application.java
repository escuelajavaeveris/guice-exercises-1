package guice.exercies.mail;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class Application {

    public static void main(String[] args) {
        Injector emailInjector = Guice.createInjector(new EmailModule());
        EmailNotification emailNotification = emailInjector.getInstance(EmailNotification.class);
        Email emailToSend = new Email("ionela.teclea@edreams.com",
                "First email", "This is the first email");
        emailNotification.sendEmailNotification(emailToSend);
    }
}
