package guice.exercies.mail;

public interface EmailChecker {
    boolean isValidEmail(String emailAddress);
}
