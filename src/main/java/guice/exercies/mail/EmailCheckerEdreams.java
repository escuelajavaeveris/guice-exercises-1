package guice.exercies.mail;

public class EmailCheckerEdreams implements EmailChecker {

    @Override
    public boolean isValidEmail(String emailAddress) {
        return emailAddress != null && emailAddress.endsWith("@edreams.com");
    }
}
