package guice.exercies.mail;

public class EmailException extends RuntimeException {
    public EmailException(String message) {
        super(message);
    }
}
