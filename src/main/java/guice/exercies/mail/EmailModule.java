package guice.exercies.mail;

import com.google.inject.AbstractModule;

public class EmailModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(EmailChecker.class).to(EmailCheckerEdreams.class);
        bind(EmailSender.class).to(EmailSenderCo.class);
    }
}
