package guice.exercies.mail;

import com.google.inject.Inject;

public class EmailNotification {

    private EmailChecker emailChecker;
    private EmailSender emailSender;

    @Inject
    public EmailNotification(EmailChecker emailChecker, EmailSender emailSender) {
        this.emailChecker = emailChecker;
        this.emailSender = emailSender;
    }

    public void sendEmailNotification(Email email) {
        if (!emailChecker.isValidEmail(email.getEmailAddress())) {
            throw new EmailException("The mail is not valid");
        }
        emailSender.sendEmail(email);
    }
}
