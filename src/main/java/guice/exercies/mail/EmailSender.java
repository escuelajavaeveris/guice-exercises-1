package guice.exercies.mail;

public interface EmailSender {
    void sendEmail(Email email);
}
