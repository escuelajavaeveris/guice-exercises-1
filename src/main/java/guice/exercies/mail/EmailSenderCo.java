package guice.exercies.mail;

public class EmailSenderCo implements EmailSender {
    @Override
    public void sendEmail(Email email) {
        System.out.println(email.toString() +  "\nIs sent from CO");
    }
}
