package guice.exercies.mail;

import org.junit.Assert;
import org.junit.Test;

public class EmailCheckerTest {

    @Test
    public void testIsValidEmailWithValidEmail() {
        EmailChecker emailChecker = new EmailCheckerEdreams();
        Assert.assertTrue(emailChecker.isValidEmail("valid_email@edreams.com"));
    }

    @Test
    public void testIsValidEmailWithInvalidEmail() {
        EmailChecker emailChecker = new EmailCheckerEdreams();
        Assert.assertFalse(emailChecker.isValidEmail("invalid_email@yahoo.com"));
    }

    @Test
    public void testIsValidEmailWithNullEmail() {
        EmailChecker emailChecker = new EmailCheckerEdreams();
        Assert.assertFalse(emailChecker.isValidEmail(null));
    }

}
