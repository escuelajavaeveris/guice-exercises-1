package guice.exercies.mail;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EmailNotificationTest {

    @Mock
    private EmailCheckerEdreams emailChecker;
    @Mock
    private EmailSenderCo emailSender;
    @Mock
    private Email email;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSendEmailNotification() {
        when(email.getEmailAddress()).thenReturn("validEmail");
        when(emailChecker.isValidEmail("validEmail")).thenReturn(true);
        EmailNotification emailNotification = new EmailNotification(emailChecker, emailSender);
        emailNotification.sendEmailNotification(email);
        verify(emailSender, times(1)).sendEmail(email);
    }

    @Test(expected = EmailException.class)
    public void testSendEmailNotificationForInvalidEmail() {
        when(email.getEmailAddress()).thenReturn("invalid Email");
        when(emailChecker.isValidEmail("invalid Email")).thenReturn(false);
        EmailNotification emailNotification = new EmailNotification(emailChecker, emailSender);
        emailNotification.sendEmailNotification(email);
    }
}
